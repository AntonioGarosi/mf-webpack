const placheholder = '???';

const list = {
  it: {
    'generic/test': 'Hello, World!'
  }
};

const strings = new class {
  constructor() {
    this.list = list;
    this.language = 'it';
  }
  setLanguage(language) {
    this.language = language;
  }
  get(key) {
    return this.list[this.language][key] ? this.list[this.language][key] : placheholder;
  }
};

export { strings };