import '../style/style.scss';
import { strings } from './strings';

const manager = new Object;
window.manager = manager;
manager.strings = strings;

document.onreadystatechange = () => {
  if (document.readyState === 'complete') {
    console.log('Page is ready');
  }
}

export {manager}